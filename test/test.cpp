#include <iostream>
#include <cmath>
#include <chrono>

using namespace std;

double fun(double x) {
  return 4 / (1 + x * x);
}

double calc_integral(size_t n) {
  const double a = 0;
  const double b = 1;
  double h = (b - a) / n;
  double h_2 = h / 2;
  double h2 = 2 * h;
  double h_6 = h / 6;
  double xi = a;
  double res = 0;

  for (size_t i = 0; i < n; i++) {
    xi += h;
    res += (fun(xi) + 4 * fun(xi - h_2) + fun(xi - h)) * h_6;
  }
  return res;
}

int main() {
  const double analyticRes = 4 * atan(1);
  chrono::steady_clock::time_point tic, toc;
  size_t n = 10;
  cout << "Integral 4/(1+x^2) 0..1 = pi = " << analyticRes << endl;
  cout << "Sequential computation:\n";
  for (size_t i = 0; i < 6; i++) {
    tic = chrono::steady_clock::now();
    double res = calc_integral(n);
    toc = chrono::steady_clock::now();

    cout << "Result for n = " << n << "\t is " << res <<
      "\tprecision = " << analyticRes - res << ", \telapsed time = " <<
      (chrono::duration_cast<chrono::nanoseconds>(toc - tic).count()) / 1.0e+9 << " seconds." << endl;
    n *= 10;
  }
}
